Title: 2023-10-18
Date: 2023-10-18 08:30
Category: 2023
Slug: gitlab-dast-api

Getting the [Dast API](https://docs.gitlab.com/ee/user/application_security/dast_api/) to work has been fun today.

There are a few options you can use to tell the analyzer what to test.  The team member working on our APIs uses Postman collections and this is one of the way you can [configure the analyzer](https://docs.gitlab.com/ee/user/application_security/dast_api/#postman-collection).

You can get the Postman collection via API if you know its uid:
```
curl -X GET https://api.getpostman.com/collections/$POSTMAN_COLLECTION_UID -H "X-Api-Key: $POSTMAN_API_KEY" -H "Cache-Control: no-cache" -o collection.json
```

However, configuring the analyzer this way did not work for me.  I kept running into:
```
Error, invalid Postman Collection. If this is a legacy v1.0 collection, please re-export as v2.0 or v2.1
```

I'm guessing this is probably related to how I extracted the collection, I suppose I could try to manually extract and see if the output is different, maybe the formatting is wrong.

What I ended up doing is using the `transformations` endpoint to convert to openapi spec ([another way to configure the analyzer](https://docs.gitlab.com/ee/user/application_security/dast_api/#configure-dast-api-with-an-openapi-specification)):
```
curl -X GET https://api.getpostman.com/collections/$POSTMAN_COLLECTION_UID/transformations -H "X-Api-Key: $POSTMAN_API_KEY" -H "Cache-Control: no-cache" -o openapi-spec.json
jq -r '.output' openapi-spec.json > openapi-spec.json
```

I didn't realize the response from the api would not be the openapi spec as is.  The spec was contained in the response and I parsed it with jq.  [This](https://editor.swagger.io) helped me realize the spec I was trying to use was wrong.

If your postman collection has variables, you might want to get yours working using the postman collection.  This is because there is a way to [populate the variables](https://docs.gitlab.com/ee/user/application_security/dast_api/#dast-api-scope-custom-json-file-format): `DAST_API_POSTMAN_COLLECTION_VARIABLES`

We had one variable to replace (-> `{{host}}`) after the conversion to openapi format, so I replaced the variable a different way:
```
sed -i "s/{{host}}/$API_HOST/g" collection.json
```

I really don't like testing my ci/cd pipelines by running a ci/cd pipeline :\ When possible, I like to pull the image being used locally and replicate the problem.  But sometimes brute forcing GitLab pipelines to work is the most practical.